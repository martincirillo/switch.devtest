﻿namespace SharpBank.Exceptions
{
	using System;

	public class AccountNotFoundException : Exception
	{
		public AccountNotFoundException(string accountId)
			: base ($"The account with ID \"{accountId}\" was not found")
		{
		}
	}
}
