﻿namespace SharpBank.Exceptions
{
	using System;

	public class InsufficientFoundsException : Exception
	{
		public InsufficientFoundsException(double amountToWithdraw, string accountId)
			: base($"You cannot withdraw {amountToWithdraw} from account {accountId}")
		{
		}
	}
}