﻿namespace SharpBank.Model
{
	using System;

	public class Transaction
	{
		public Transaction(double amount)
		{
			this.Amount = amount;
			this.Date = DateProvider.GetInstance().Now();
		}

		public double Amount { get; private set; }

		public DateTime Date { get; private set; }

		public bool IsDeposit()
		{
			return this.Amount > 0;
		}
	}
}
