﻿namespace SharpBank.Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using SharpBank.Exceptions;

	public class Account
	{
		public enum Type
		{
			Checking = 0,
			Savings = 1,
			MaxiSavings = 2,
		}

		public Account(Type accountType)
		{
			this.AccountType = accountType;
			this.Id = Guid.NewGuid().ToString();
			this.TotalSavings = 0;
		}

		public Type AccountType { get; private set; }

		public string Id { get; private set; }

		public List<Transaction> Transactions { get; private set; } = new List<Transaction>();

		public double TotalSavings { get; private set; }

		public void Deposit(double amount)
		{
			if (amount <= 0)
			{
				throw new ArgumentException("amount must be greater than zero");
			}
			else
			{
				this.Transactions.Add(new Transaction(amount));
				this.TotalSavings += amount;
			}
		}

		public void Withdraw(double amount)
		{
			if (amount <= 0)
			{
				throw new ArgumentException("amount must be greater than zero");
			}
			else if (amount > this.TotalSavings)
			{
				throw new InsufficientFoundsException(amount, this.Id);
			}
			else
			{
				this.Transactions.Add(new Transaction(-amount));
				this.TotalSavings -= amount;
			}
		}

		public double InterestEarned()
		{
			double amount = this.TotalSavings;
			switch (this.AccountType)
			{
				case Type.Savings:
					if (amount <= 1000)
						amount *= 0.001;
					else
						amount = 1 + (amount - 1000) * 0.002;
					break;

				case Type.MaxiSavings:
					if (this.AreWithdrawlsInPastDays(10))
						amount *= 0.001;
					else
						amount *= 0.05;
					break;

				default:
					amount *= 0.001;
					break;
			}

			// Interest rates should accrue daily (incl. weekends), rates MaxiSavings are per-annum
#warning This requirement (Exercise #3) might have been misunderstood. The given requirement might not have the required information to truly achieve this feature as expected

			Transaction lastTransaction = this.Transactions.OrderByDescending(transaction => transaction.Date).FirstOrDefault();
			if (lastTransaction != null)
			{
				DateTime now = DateProvider.GetInstance().Now();
				int times = 1;
				TimeSpan difference = now.Subtract(lastTransaction.Date);
				if (this.AccountType == Type.MaxiSavings)
				{
					times = new DateTime(difference.Ticks).Year - 1;
				}
				else
				{
					times = (int)difference.TotalDays;
				}

				amount *= 1 + times;
			}
			
			return amount;
		}

		private bool AreWithdrawlsInPastDays(int days)
		{
			DateTime now = DateProvider.GetInstance().Now();
			return this.Transactions.Where(
				transaction =>
					!transaction.IsDeposit() &&
					transaction.Date.AddDays(days) >= now).Count() > 0;
		}
	}
}
