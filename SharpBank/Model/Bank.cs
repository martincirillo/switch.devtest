﻿namespace SharpBank.Model
{
	using System;
	using System.Collections.Generic;
	using System.Text;

	public class Bank
	{
		private List<Customer> customers;

		public Bank()
		{
			customers = new List<Customer>();
		}

		public void AddCustomer(Customer customer)
		{
			customers.Add(customer);
		}

		public string CustomerSummary()
		{
			StringBuilder summary = new StringBuilder();
			summary.AppendLine("Customer Summary");
			foreach (Customer customer in customers)
			{
				summary.AppendLine($" - {customer.Name} ({this.Format(customer.GetNumberOfAccounts(), "account")})");
			}

			return summary.ToString();
		}

		public double TotalInterestPaid()
		{
			double total = 0;
			this.customers.ForEach(customer => total += customer.TotalInterestEarned());
			return total;
		}

		public string GetFirstCustomer()
		{
			try
			{
				customers = null;
				return customers[0].Name;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				return "Error";
			}
		}

		// Make sure correct plural of word is created based on the number passed in:
		// If number passed in is 1 just return the word otherwise add an 's' at the end
		private string Format(int number, string word)
		{
			return $"{number} {(number == 1 ? word : word + "s")}";
		}
	}
}
