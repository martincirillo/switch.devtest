﻿namespace SharpBank.Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using SharpBank.Exceptions;

	public class Customer
	{
		private List<Account> accounts;

		public Customer(string name)
		{
			this.Name = name;
			this.accounts = new List<Account>();
		}

		public string Name { get; private set; }

		public void OpenAccount(Account account)
		{
			this.accounts.Add(account);
		}

		public int GetNumberOfAccounts()
		{
			return this.accounts.Count;
		}

		public double TotalInterestEarned()
		{
			double total = 0;
			this.accounts.ForEach(account => total += account.InterestEarned());
			return total;
		}

		public void TransferFounds(string accountFromId, string accountToId, double amount)
		{
			if (string.IsNullOrWhiteSpace(accountFromId))
			{
				throw new AccountNotFoundException(accountFromId);
			}

			if (string.IsNullOrWhiteSpace(accountToId))
			{
				throw new AccountNotFoundException(accountToId);
			}

			Account accountFrom = this.accounts.SingleOrDefault(account => account.Id == accountFromId);
			if (accountFrom == null)
			{
				throw new AccountNotFoundException(accountFromId);
			}

			Account accountTo = this.accounts.SingleOrDefault(account => account.Id == accountToId);
			if (accountTo == null)
			{
				throw new AccountNotFoundException(accountToId);
			}

			accountFrom.Withdraw(amount);
			accountTo.Deposit(amount);
		}

		/// <summary>
		/// Gets a statement
		/// </summary>
		/// <returns></returns>
		public string GetStatement()
		{
			// JIRA-123 Change by Joe Bloggs 29/7/1988 start
			// reset statement to null here
			// JIRA-123 Change by Joe Bloggs 29/7/1988 end

			StringBuilder statement = new StringBuilder();
			statement.AppendLine($"Statement for {this.Name}");
			double total = 0.0;
			foreach (Account account in this.accounts)
			{
				statement.AppendLine();
				statement.AppendLine(this.StatementForAccount(account));
				total += account.TotalSavings;
			}

			statement.AppendLine();
			statement.Append($"Total In All Accounts {this.ToDollars(total)}");
			return statement.ToString();
		}

		private string StatementForAccount(Account account)
		{
			StringBuilder statement = new StringBuilder();

			switch (account.AccountType)
			{
				case Account.Type.Checking:
					statement.AppendLine("Checking Account");
					break;
				case Account.Type.Savings:
					statement.AppendLine("Savings Account");
					break;
				case Account.Type.MaxiSavings:
					statement.AppendLine("Maxi Savings Account");
					break;
			}

			// Now total up all the transactions
			foreach (Transaction transaction in account.Transactions)
			{
				statement.AppendLine($"  {(transaction.Amount < 0 ? "withdrawal" : "deposit")} {this.ToDollars(transaction.Amount)}");
			}

			statement.AppendLine($"Total {this.ToDollars(account.TotalSavings)}");
			return statement.ToString();
		}

		private string ToDollars(double d)
		{
			return string.Format("${0:N2}", Math.Abs(d));
		}
	}
}
