﻿namespace SharpBank.Test
{
	using System.Text;
	using NUnit.Framework;
	using SharpBank.Exceptions;
	using SharpBank.Model;

	[TestFixture]
	public class CustomerTest
	{
		[Test]
		public void TestCustomerStatementGeneration()
		{
			Account checkingAccount = new Account(Account.Type.Checking);
			Account savingsAccount = new Account(Account.Type.Savings);

			Customer henry = new Customer("Henry");
			henry.OpenAccount(checkingAccount);
			henry.OpenAccount(savingsAccount);

			checkingAccount.Deposit(100.0);
			savingsAccount.Deposit(4000.0);
			savingsAccount.Withdraw(200.0);

			StringBuilder statement = new StringBuilder();
			statement.AppendLine("Statement for Henry");
			statement.AppendLine();
			statement.AppendLine("Checking Account");
			statement.AppendLine("  deposit $100.00");
			statement.AppendLine("Total $100.00");
			statement.AppendLine();
			statement.AppendLine();
			statement.AppendLine("Savings Account");
			statement.AppendLine("  deposit $4,000.00");
			statement.AppendLine("  withdrawal $200.00");
			statement.AppendLine("Total $3,800.00");
			statement.AppendLine();
			statement.AppendLine();
			statement.Append("Total In All Accounts $3,900.00");
			Assert.AreEqual(statement.ToString(), henry.GetStatement());
		}

		[Test]
		public void TestOneAccount()
		{
			Customer oscar = new Customer("Oscar");
			oscar.OpenAccount(new Account(Account.Type.Savings));
			Assert.AreEqual(1, oscar.GetNumberOfAccounts());
		}

		[Test]
		public void TestTwoAccount()
		{
			Customer oscar = new Customer("Oscar");
			oscar.OpenAccount(new Account(Account.Type.Savings));
			oscar.OpenAccount(new Account(Account.Type.Checking));
			Assert.AreEqual(2, oscar.GetNumberOfAccounts());
		}

		[Test]
		public void TestThreeAcounts()
		{
			Customer oscar = new Customer("Oscar");
			oscar.OpenAccount(new Account(Account.Type.Savings));
			oscar.OpenAccount(new Account(Account.Type.Checking));
			oscar.OpenAccount(new Account(Account.Type.MaxiSavings));
			Assert.AreEqual(3, oscar.GetNumberOfAccounts());
		}

		[Test]
		public void TestTransferFounds_AccountFromIdEmpty()
		{
			Customer oscar = new Customer("Oscar");
			Assert.Throws<AccountNotFoundException>(() => oscar.TransferFounds(string.Empty, string.Empty, 100));
		}

		[Test]
		public void TestTransferFounds_AccountToIdEmpty()
		{
			Customer oscar = new Customer("Oscar");
			Assert.Throws<AccountNotFoundException>(() => oscar.TransferFounds(string.Empty, string.Empty, 100));
		}

		[Test]
		public void TestTransferFounds_AccountFromNotExists()
		{
			Customer oscar = new Customer("Oscar");
			Assert.Throws<AccountNotFoundException>(() => oscar.TransferFounds("non-existing", string.Empty, 100));
		}

		[Test]
		public void TestTransferFounds_AccountToNotExists()
		{
			Customer oscar = new Customer("Oscar");
			Account accountFrom = new Account(Account.Type.Savings);
			oscar.OpenAccount(accountFrom);
			Assert.Throws<AccountNotFoundException>(() => oscar.TransferFounds(accountFrom.Id, "non-existing", 100));
		}

		[Test]
		public void TestTransferFounds_InsufficientFounds()
		{
			Customer oscar = new Customer("Oscar");
			Account accountFrom = new Account(Account.Type.Savings);
			Account accountTo = new Account(Account.Type.Savings);
			oscar.OpenAccount(accountFrom);
			oscar.OpenAccount(accountTo);
			Assert.Throws<InsufficientFoundsException>(() => oscar.TransferFounds(accountFrom.Id, accountTo.Id, 100));
		}

		[Test]
		public void TestTransferFounds_OK()
		{
			Customer oscar = new Customer("Oscar");
			Account accountFrom = new Account(Account.Type.Savings);
			Account accountTo = new Account(Account.Type.Savings);
			oscar.OpenAccount(accountFrom);
			oscar.OpenAccount(accountTo);
			accountFrom.Deposit(100);
			oscar.TransferFounds(accountFrom.Id, accountTo.Id, 100);

			Assert.IsTrue(accountFrom.TotalSavings == 0);
			Assert.IsTrue(accountTo.TotalSavings == 100);
		}
	}
}
