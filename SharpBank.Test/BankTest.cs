﻿namespace SharpBank.Test
{
	using System.Text;
	using NUnit.Framework;
	using SharpBank.Model;

	[TestFixture]
	public class BankTest
	{
		private static readonly double DoubleDelta = 1e-15;

		[Test]
		public void CustomerSummary()
		{
			Bank bank = new Bank();
			Customer john = new Customer("John");
			john.OpenAccount(new Account(Account.Type.Checking));
			bank.AddCustomer(john);

			StringBuilder summary = new StringBuilder();
			summary.AppendLine("Customer Summary");
			summary.AppendLine(" - John (1 account)");
			Assert.AreEqual(summary.ToString(), bank.CustomerSummary());
		}

		[Test]
		public void CheckingAccount()
		{
			Bank bank = new Bank();
			Account checkingAccount = new Account(Account.Type.Checking);
			Customer bill = new Customer("Bill");
			bill.OpenAccount(checkingAccount);
			bank.AddCustomer(bill);

			checkingAccount.Deposit(100.0);

			Assert.AreEqual(0.1, bank.TotalInterestPaid(), DoubleDelta);
		}

		[Test]
		public void SavingsAccount()
		{
			Bank bank = new Bank();
			Account checkingAccount = new Account(Account.Type.Savings);
			Customer bill = new Customer("Bill");
			bill.OpenAccount(checkingAccount);
			bank.AddCustomer(bill);

			checkingAccount.Deposit(1500.0);

			Assert.AreEqual(2.0, bank.TotalInterestPaid(), DoubleDelta);
		}

		[Test]
		public void MaxiSavingsAccount()
		{
			Bank bank = new Bank();
			Account checkingAccount = new Account(Account.Type.MaxiSavings);
			Customer bill = new Customer("Bill");
			bill.OpenAccount(checkingAccount);
			bank.AddCustomer(bill);

			checkingAccount.Deposit(3000.0);

			Assert.AreEqual(150.0, bank.TotalInterestPaid(), DoubleDelta);
		}

	}
}
