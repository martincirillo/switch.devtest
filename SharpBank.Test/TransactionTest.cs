﻿namespace SharpBank.Test
{
	using NUnit.Framework;
	using SharpBank.Model;

	[TestFixture]
    public class TransactionTest
    {
        [Test]
        public void Transaction()
        {
            Transaction transaction = new Transaction(5);
            Assert.AreEqual(true, transaction is Transaction);
        }
    }
}
